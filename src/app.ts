$((): void => {
  let app: angular.IModule = angular.module('app', ['ui.router', 'ngMaterial']);

  app.config(['$urlRouterProvider', '$stateProvider', ($urlRouterProvider: ng.ui.IUrlRouterProvider,
                                                       $stateProvider: ng.ui.IStateProvider) => {
    $urlRouterProvider.otherwise('first');

    $stateProvider.state('first', {
      url: '/first',
      template: main.first.firstHtmlTemplate
    })
    .state('second', {
      url: '/second',
      template: main.second.secondHtmlTemplate
    });

  }]);

  app.run(() => {
    // run
  });

  angular.bootstrap(document, ['app']);
});
