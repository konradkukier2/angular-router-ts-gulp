var gulp = require('gulp');
var ts = require('gulp-typescript');
var merge = require('merge2');
var clean = require('gulp-clean');

gulp.task('compileTs', function() {
  var tsProject = ts.createProject('tsconfig.json');
	var tsResult = tsProject.src()
		.pipe(ts(tsProject));
 
	return merge([
		tsResult.dts.pipe(gulp.dest('release')),
		tsResult.js.pipe(gulp.dest('release'))
	]);
});

 
gulp.task('clean', function () {
	return gulp.src('release', {read: false})
		.pipe(clean());
});

gulp.task('watchTs', function () {
  gulp.watch(['./src/**/*.ts'], ['default']);
});

gulp.task('default', ['compileTs', 'clean']);